<?php
/**
 * Module Entry Point
 * 
 * @package    Joomla.Tutorials
 * @subpackage Modules
 * @license    GNU/GPL, see LICENSE.php
 * @link       https://gitlab.com/gratia-mira-opensource/kundenhinweis-ausland
 * mod_kundenhinweis is a free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

/**
 * https://gist.github.com/geerlingguy/a438b41a9a8f988ee106 
 * Check if the given user agent string is one of a crawler, spider, or bot.
 *
 * @param string $user_agent
 *   A user agent string (e.g. Googlebot/2.1 (+http://www.google.com/bot.html))
 *
 * @return bool
 *   TRUE if the user agent is a bot, FALSE if not.
 */
function smart_ip_detect_crawler() {
  // User lowercase string for comparison.
  $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);

  // A list of some common words used only for bots and crawlers.
  $bot_identifiers = array(
    'bot',
    'slurp',
    'crawler',
    'spider',
    'curl',
    'facebook',
    'fetch',
  );

  // See if one of the identifiers is in the UA string.
  foreach ($bot_identifiers as $identifier) {
    if (strpos($user_agent, $identifier) !== FALSE) {
      return TRUE;
    }
  }

  return FALSE;
}

$IstBot = smart_ip_detect_crawler();

// Wenn kein Cookie generiert wurde
// Und es kein Bot isset
if(!isset($_COOKIE['InfoAuslandKunde']) 
   and $IstBot == FALSE) {
	// Und der Datenschutzhinweis schon angezeigt wurde (https://gitlab.com/gratia-mira-opensource/cookie-hinweis-light)
	// Und eine IP Adresse vorhanden ist
	// Und die Webesite nicht von einem externen Link aufgerufen wurde (Keine lange Ladezeit für Erstbesucher)
	if(isset($_COOKIE['cookies'])
		and !empty($_SERVER['REMOTE_ADDR'])
		and strpos($_SERVER['HTTP_REFERER'], JURI::base()) !== FALSE) {

		// Daten ausgeben
		require JModuleHelper::getLayoutPath('mod_kundenhinweis');
	}
}
?>
