<?php 
// No direct access
defined('_JEXEC') or die; ?>

<style>
	#IP-Adresse {
		font-size: 0px;
	}
	.HinweisKunde {
		position:fixed;
		top:0;
		left:0;
		width:100vw;
		height: auto;
		z-index: 99999 !important;
		color: white;
		font-size: 30px;
		background:red;
		border: none;
		padding:10px;
	}
</style>
<body>
	<div id="IP-Adresse"><?php echo $_SERVER['REMOTE_ADDR']; ?></div>
	<script type="text/javascript">
		<?php include dirname(__FILE__,2) . '/helper.js'; ?>
	</script>
	<div id='Hinweis'></div>
</body>
