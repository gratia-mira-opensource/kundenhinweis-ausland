window.onload = function () {
    // IP Adresse holen
	let html = document.getElementById('IP-Adresse').outerHTML;
	let IP = html.replace(/<[^>]*>?/gm, '');
	// zum Test auf Localhost
    // var IP = '95.174.67.50';

	console.log(IP);

    if (IP) {

        let Pfad = "https://ip2c.org/?ip=" + IP;
        let Land = httpGet(Pfad);

        function httpGet(Pfad){
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", Pfad, false ); // false for synchronous request
            xmlHttp.send( null );
            return xmlHttp.responseText.split(';');
        }
        
        console.log(Land);

        if (Land.slice(3) && Land.slice(3) != 'WRONG INPUT') {

            // Ablaufdatum generieren
			// 2629700000 gleich Milisekunden in einem Monat
            const Zeit = new Date();
            Zeit.setTime(Zeit.getTime() + (3 * 2629700000));
                var Ablaufdatum = Zeit.toUTCString();

            if (Land.slice(3) == "Switzerland") {
                // es soll kein Text angezeigt werden
                document.cookie = 'InfoAuslandKunde=nicht prüfen bis ' +  Ablaufdatum + '; expires=' + Ablaufdatum + '; path=/';
            } else {
                var Info = "<div class='HinweisKunde'><center><p>Sie möchten vermutlich aus dem Ausland bestellen? Schön! Alle wichtigen Infos finden Sie unter diesem <a href='https://www.edition-nehemia.ch/j3/agb.html#Lieferung' target='new' style='color: yellow;'>Link!</a></p></center></div>";
                document.getElementById("Hinweis").innerHTML = Info;
                document.cookie = 'InfoAuslandKunde=informiert bis ' +  Ablaufdatum + '; expires=' + Ablaufdatum + '; path=/';
            }
        }
    }
}
