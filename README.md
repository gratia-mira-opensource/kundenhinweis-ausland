# Kundenhinweis Ausland

Nicht Schweizer Kunden wird ein Hinweis angezeigt, wenn sie die Webseite besuchen. Dieses Modul ist für Joomla geschrieben.

# Abhänigkeiten

Das Modul läuft nur, wenn auch das Modul [Cookie-Hinweis-Light (mod_datenschutzhinweis)](https://gitlab.com/gratia-mira-opensource/cookie-hinweis-light) installiert ist und das Cookie "cookies" gesetzt ist.
