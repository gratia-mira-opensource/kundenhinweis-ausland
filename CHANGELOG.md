# 1.0.0 15.05.2021
* Erstinstallation
# 1.0.1 05.06.2021
* Fehler: Cookie wird beim dritten Versuch doch wieder angezeigt.
* Fehler: Hatte keine Lizenz für die Landermittlung!
# 1.0.4 11.11.2021
* Verbesserung: Cookiehinweis (mod_datenschutzhinweis) und Kundenhinweis werden nicht parallel angezeigt. Damit entsteht eine Abhänigkeit zum Modul mod_datenschutzhinweis!  
* Verbesserung: Die IP-Adresse wird erst nach dem Laden der Seite mittels Javascript geprüft.
* Verbesserung: Entweder es wird ein Land erkannt, oder es wird keine Meldung gemacht, bzw. Cookie gesetzt.
# 1.0.5 09.04.2022
* Fehler: Ablaufdatum Cookie funktioniert jetzt korrekt.
* Verbesserung: Ist es ein Bot, wird keine Land-Abfrage gemacht. Auch wenn keine IP-Adresse vorhanden ist.
* Verbesserung: Nicht anzeigen, wenn der Besucher die Seite zum ersten Mal abruft.
* Verbesserung: Cookie hat besseren Inhalt.
